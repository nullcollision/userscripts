// ==UserScript==
// @name         Convert PSPrices (US) to Euro
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Converts prices to Euro on PSPrices (Hong Kong)
// @author       Nullcollision
// @match        https://psprices.com/region-us/*
// @grant        none
// ==/UserScript==


var allelements = document.getElementsByClassName('content__game_card__price').length;

let currency = {};

let calculate = () => {
    for (var i = 0; i < allelements; i++) {
        var spanclass = document.getElementsByClassName('content__game_card__price')[i];
        var original = spanclass.getAttribute('content');
        var converted = Math.round((original/currency.rates.USD)*100) / 100;

        document.getElementsByClassName('content__game_card__price')[i].innerHTML += '|  <b>' +converted +'€</b>';

    }

}

fetch('https://api.exchangeratesapi.io/latest')
    .then((resp) => resp.json())
    .then((data) => currency.rates = data.rates)
    .then(calculate);



